# coding: utf8

T.force('es')

import os

dt = db.define_table

dt('sede',
   Field('nombre'),
   Field('direccion'),
   Field('lat', 'double', readable = False, writable = False),
   Field('lon', 'double', readable = False, writable = False)
)


dt('evento',
   Field('sede', 'reference sede'),
   Field('nombre'),
   Field('lugar'),
   Field('inicio', 'datetime'),
   Field('fin', 'datetime'),
   format = lambda r: '%(nombre)s %(fecha)s' % dict(
       nombre = r.nombre,
       fecha = '%s-%s' % (r.fecha_inicio.date().year,
                          r.fecha_inicio.date().month)
   )
)

dt('expositor',
   Field('usuario', 'reference auth_user'),
   Field('email'),
   Field('web'),
)

dt('actividad',
   Field('evento', 'reference evento'),
   Field('tipo',
         requires=IS_IN_SET([(1,'Charla'),(2, 'Taller')])),
   Field('nombre'),
   Field('duracion', 'integer', comment=T('Minutos de duración')),
   Field('fecha'),
   Field('confirmado', 'boolean'),
   Field('descripcion', 'text'),
)

dt('documento_actividad',
   Field('actividad', 'reference actividad'),
   Field('documento', 'upload',
         uploadfolder = os.path.join(request.folder,'static/uploads'),
         uploadseparate = True
     ), 
   Field('nombre'),
)


dt('asistente',
   Field('usuario', 'reference auth_user'),
   Field('actividad', 'reference actividad'),
   Field('confirmado', 'boolean', readable=False, writable=False)
)
